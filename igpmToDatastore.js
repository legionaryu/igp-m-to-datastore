"use strict";

var exporter = require("datastore-exporter");
var moment = require("moment-timezone");
var path = require("path");
var igpm_data = require("./igpm-bcb.json");

var dirPath = path.join("economics","igpm", "monthly");
var regex = new RegExp("\\" + path.sep, "g");

var arr = igpm_data.dataset.data;

console.log("for on", arr.length);
for(var i=arr.length-1; i>=0; i--) {
    // console.log("i:", i, "arr[i]:", arr[i]);
    var date = new moment.tz(arr[i][0], "YYYY-MM-DD", "America/Sao_Paulo").hour(23);
    var dayOne = date.clone().date(1).hour(0);
    console.log(exporter.writeToDatastoreLog(dirPath, dayOne, dirPath.replace(regex,"."), [arr[i][1]], "igp-m:%f"));
    console.log(exporter.writeToDatastoreLog(dirPath, date, dirPath.replace(regex,"."), [arr[i][1]], "igp-m:%f"));
}
console.log("post for");
